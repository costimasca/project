<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <title>Login page</title>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>
<h1>Add new user </h1>

<form:form method="POST" commandName="user" action="${pageContext.request.contextPath}/admin/add-user" >
    <table>
        <tbody>
        <tr>
            <td>username:</td>
            <td><form:input path="username" /></td>
            <td><form:errors path="username" cssStyle="color: red;"/></td>
        </tr>
        <tr>
            <td>password:</td>
            <td><form:input path="password" /></td>
            <td><form:errors path="password" cssStyle="color: red;"/></td>
        </tr>

        <tr>
            <td>role:</td>
            <td><form:input path="role" /></td>
            <td><form:errors path="role" cssStyle="color: red;"/></td>
        </tr>

        <tr>
            <td><input type="submit" value="Create" /></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>
</form:form>
</body>
</html>