<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: constantin
  Date: 12/18/17
  Time: 6:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User List Page</title>
</head>
<body>
<h1>User List Page</h1>
<table style="text-align: center;" border="1px" cellpadding="0" cellspacing="0" >
    <thead>
    <tr>
        <th width="25px">id</th><th width="100px">username</th><th width="100px">Password</th>
        <th width="50px">actions</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="user" items="${userList}">
        <tr>
            <td>${user.userId}</td>
            <td>${user.username}</td>
            <td>${user.password}</td>
            <td>
                <a href="${pageContext.request.contextPath}/admin/edit/${user.userId}.html">Edit</a><br/>
                <a href="${pageContext.request.contextPath}/admin/delete-user/${user.userId}.html">Delete</a><br/>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<a href="${pageContext.request.contextPath}/">Home page</a>
<a href="<c:url value="/j_spring_security_logout" />" >Logout</a> <br/>

</body>
</html>
