package com.spr.repository;

import com.spr.model.Meal;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by vlad on 19.12.2017.
 */


public interface MealRepository extends JpaRepository<Meal,Integer> {

}