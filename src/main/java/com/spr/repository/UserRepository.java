package com.spr.repository;

import com.spr.model.Meal;
import com.spr.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by constantin on 12/18/17.
 */
public interface UserRepository extends JpaRepository<User,Integer> {
    public User findByUsername(String username);
    public User findByUserId(Integer id);

}
