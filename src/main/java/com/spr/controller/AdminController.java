package com.spr.controller;

import com.spr.model.Meal;
import com.spr.model.User;
import com.spr.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by constantin on 12/2/17.
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @RequestMapping(value = "/")
    public ModelAndView adminPage() {
        ModelAndView mav = new ModelAndView("admin");
        return mav;
    }

    @RequestMapping(value = "/viewallusers")
    public ModelAndView viewAll() {
        ModelAndView mav = new ModelAndView("user-list");
        List<User> userList = adminService.findAll();
        mav.addObject("userList", userList);
        return mav;
    }

    @RequestMapping(value = "/viewallmeals")
    public ModelAndView viewAllMeals() {
        ModelAndView mav = new ModelAndView("meal-list");
        List<Meal> mealList = adminService.findAllMeals();
        mav.addObject("mealList", mealList);
        return mav;
    }

    @RequestMapping(value = "/add-user", method = RequestMethod.GET )
    public ModelAndView addUser(){
        ModelAndView mav = new ModelAndView("add-user");
        mav.addObject(new User());
        return mav;
    }


    @RequestMapping(value="/add-user",method=RequestMethod.POST)
    public ModelAndView addUser(@ModelAttribute @Valid User user, BindingResult result,
                                final RedirectAttributes redirectAttributes) {

        ModelAndView mav = new ModelAndView();
        String message = "New Employee " + user.getUsername() + " was successfully added!";

        adminService.addUser(user);
        mav.setViewName("redirect:/admin/viewallusers");
        return mav;
    }

    @RequestMapping(value="/delete-user/{id}", method=RequestMethod.GET)
    public ModelAndView deleteEmployee(@PathVariable Integer id){
        ModelAndView mav = new ModelAndView("redirect:/admin/viewallusers");

        adminService.deleteUser(id);
        return mav;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public ModelAndView editUserPage(@PathVariable Integer id) {
        ModelAndView mav = new ModelAndView("user-edit");

        User user = adminService.findById(id);
        mav.addObject("user", user);
        return mav;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
    public ModelAndView editUser(@ModelAttribute @Valid User user,
                                 BindingResult result,
                                 @PathVariable Integer id,
                                 final RedirectAttributes redirectAttributes){
        System.out.println(user.getUserId());
        user.setUserId(id);
        System.out.println(user.getUserId());
        ModelAndView mav = new ModelAndView("redirect:/admin/viewallusers");
        String message = "User was successfully updated.";
        User updated = adminService.update(user);

        redirectAttributes.addFlashAttribute("message", message);
        return mav;
    }




}