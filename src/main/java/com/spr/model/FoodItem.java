package com.spr.model;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by vlad on 19.12.2017.
 */

@Entity
@Table(name = "food_item", catalog = "dietdb") //inert unique constraints
public class FoodItem {

    Integer foodItemId;
    String foodItemName;
    Integer foodItemCalories;

    public FoodItem(){
    }

    public FoodItem(Integer foodItemCalories, Integer foodItemId, String foodItemName){
        this.foodItemId = foodItemId;
        this.foodItemName = foodItemName;
        this.foodItemCalories = foodItemCalories;
    }


    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = IDENTITY)
    public Integer getFoodItemId() {
        return foodItemId;
    }

    public void setFoodItemId(Integer foodItemId) {
        this.foodItemId = foodItemId;
    }

    @Column(name = "name", nullable = false)
    public String getFoodItemName() {
        return foodItemName;
    }

    public void setFoodItemName(String foodItemName) {
        this.foodItemName = foodItemName;
    }

    @Column(name = "calories", nullable = false)
    public Integer getFoodItemCalories() {
        return foodItemCalories;
    }

    public void setFoodItemCalories(Integer foodItemCalories) {
        this.foodItemCalories = foodItemCalories;
    }
}
