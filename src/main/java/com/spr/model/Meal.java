package com.spr.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by constantin on 12/18/17.
 */

@Entity
@Table(name = "meal", catalog = "dietdb")
public class Meal {

    private Integer mealId;
    private Integer totalCalories;
    private String mealType;
    private Set<User> stocks = new HashSet<User>(0);


    public Meal() {
    }

    public Meal(Integer totalCalories, String mealType) {
        this.totalCalories = totalCalories;
        this.mealType = mealType;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getMealId() {
        return mealId;
    }

    public void setMealId(Integer mealId) {
        this.mealId = mealId;
    }

    @Column(name = "total_calories", nullable = false)
    public Integer getTotalCalories() {
        return totalCalories;
    }

    public void setTotalCalories(Integer totalCalories) {
        this.totalCalories = totalCalories;
    }

    @Column(name = "meal_type", nullable = false)
    public String getMealType() {
        return mealType;
    }

    public void setMealType(String mealType) {
        this.mealType = mealType;
    }

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "meals")
    public Set<User> getStocks() {
        return stocks;
    }

    public void setStocks(Set<User> stocks) {
        this.stocks = stocks;
    }
}

