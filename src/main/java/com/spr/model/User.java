package com.spr.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by constantin on 12/18/17.
 */

@Entity
@Table(name = "user", catalog = "dietdb") //inert unique constraints
public class User {

    @Column(name = "id")
    private Integer userId;
    private String username;
    private String password;
    private Integer role;
    private Integer height;
    private Integer weight;
    private Integer age;
    private String sex;
    private Set<Meal> meals = new HashSet<Meal>(0);

    public User() {
    }

    public User(String username, String password, Integer role, Integer height, Integer weight, Integer age, String sex, Set<Meal> meals) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.height = height;
        this.weight = weight;
        this.age = age;
        this.sex = sex;
        this.meals = meals;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = IDENTITY)
    public Integer getUserId(){
        return this.userId;
    }


    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "username", unique = true, nullable = false, length = 20)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password", nullable = false, length = 20)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "role", nullable = false)
    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    @Column(name = "height")
    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Column(name="weight")
    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @Column(name = "age")
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Column(name = "sex")
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "user_meal", catalog = "dietdb", joinColumns = {
            @JoinColumn(name = "user_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "meal_id",
                    nullable = false, updatable = false) })
    public Set<Meal> getMeals() {
        return meals;
    }

    public void setMeals(Set<Meal> meals) {
        this.meals = meals;
    }
}
