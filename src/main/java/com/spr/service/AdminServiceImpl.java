package com.spr.service;

import com.spr.model.Meal;
import com.spr.model.User;
import com.spr.repository.MealRepository;
import com.spr.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by constantin on 12/18/17.
 */
@Service
public class AdminServiceImpl implements AdminService {

    @Resource
    private UserRepository userRepository;
    @Resource
    private MealRepository mealRepository;


    @Override
  //  @Transactional
    public List<User> findAll(){
        return userRepository.findAll();
    }

    @Override
    public List<Meal> findAllMeals() { return mealRepository.findAll(); }

    @Override
    @Transactional
    public User addUser(User user) { return userRepository.save(user); }

    @Override
    public void deleteUser(Integer id) { userRepository.delete(id);}

    @Override
    public User findById(Integer id) { return userRepository.findByUserId(id); }

    public User update(User user) { return userRepository.save(user); }

}
