package com.spr.service;

import com.spr.model.Meal;
import com.spr.model.User;

import java.util.List;

/**
 * Created by constantin on 12/18/17.
 */
public interface AdminService {

    public List<User> findAll();

    public List<Meal> findAllMeals();

    public User addUser(User user);

    public void deleteUser(Integer id);

    public User findById(Integer id);

    public User update(User user);
}
