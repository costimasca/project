<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: constantin
  Date: 12/18/17
  Time: 6:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Meal List Page</title>
</head>
<body>
<h1>Meal List Page</h1>
<table style="text-align: center;" border="1px" cellpadding="0" cellspacing="0" >
    <thead>
    <tr>
        <th width="25px">id</th><th width="100px">mealType</th><th width="100px">mealCalories</th>
        <th width="50px">actions</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="meal" items="${mealList}">
        <tr>
            <td>${meal.mealId}</td>
            <td>${meal.mealType}</td>
            <td>${meal.totalCalories}</td>
            <td>
                <a href="${pageContext.request.contextPath}/admin/edit/${meal.mealId}.html">Edit</a><br/>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<a href="${pageContext.request.contextPath}/">Home page</a>
<a href="<c:url value="/j_spring_security_logout" />" >Logout</a> <br/>

</body>
</html>
