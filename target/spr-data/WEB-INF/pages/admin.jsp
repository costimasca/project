<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: constantin
  Date: 12/2/17
  Time: 7:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Page</title>
</head>
<body>

<a href="${pageContext.request.contextPath}/admin/viewallusers">View Users</a>
<a href="${pageContext.request.contextPath}/admin/viewallmeals">View Meals</a>
<a href="${pageContext.request.contextPath}/admin/add-user">Add a new user</a>

<a href="${pageContext.request.contextPath}/">Home page</a>
<a href="<c:url value="/j_spring_security_logout" />" >Logout</a> <br/>

</body>
</html>
